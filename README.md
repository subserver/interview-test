Please import database.sql into a local mysql database for testing the application.

You will find the database connection details located in the settings.php file in the root directory of this application.

The database is designed to allow the intergration of more languages and translations for each phrase, however the Interface is only setup to map translations from English to Dutch.