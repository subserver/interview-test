<?php
    if(!isset($_GET['phrase'])){
        die("Phrase not set.");
    }
    $phraseid = $_GET['phrase'];
    $phrase = $phraseManager->getById($phraseid);
?>

<div>
    <span>Please translate the following phrase into Dutch: <br> '<?php echo $phrase->english; ?>'</span><br>
    <textarea id="phrase-translation" rows="4" cols="50" placeholder="Translation..."><?php echo $phrase->translation; ?></textarea><br>
    <button id="save-translation-button">Save Translation</button>
</div>

<script>
    //Save translation to database
    $('#save-translation-button').click((e) => {
        data = {
            phrase: <?php echo $phraseid; ?>,
            translation: $("#phrase-translation").val()
        }

        $.post("ajax/savetranslation.ajax.php", data ,function(data){
            alert("Translation saved!");
        })
        .fail((xhr) => {
            alert("Failed to save translation!");
        });
    });
</script>