<div>
    <h3>Please enter an english phrase, a translation will be shown if it is in the dictionary</h3>
    <span id="suggestion"></span><br>
    <textarea id="english-textarea" rows="4" cols="30" placeholder="English phrase..."></textarea>
    <span> Translates to </span>
    <textarea id="dutch-textarea" rows="4" cols="30" placeholder="Waiting to for a phrase that can be translated" disabled></textarea><br>
</div>

<script>
var suggested = null;

$("#english-textarea").on('keyup', function(e) {
    handleInputChange();
});

$(document).on("click", "#suggested-phrase", (e) => {
    $("#suggestion").html("");
    $('#english-textarea').val(suggested.english);
    handleInputChange();

})

function handleInputChange(){
    phrase = $("#english-textarea").val();
    if(phrase.length >= 3){
        data = {
            phrase: phrase
        }

        $.post("ajax/fetchtranslation.ajax.php", data ,function(data){
            data = JSON.parse(data);
            console.log(data);
            $('#dutch-textarea').text(data.translation);
        })
        .fail((xhr) => {
            var data = JSON.parse(xhr.responseText);
            console.log(data.suggestion);
            $("#suggestion").html("Do you mean '<a href=\"#\" id=\"suggested-phrase\">" + data.suggestion.english + "</a>'?");
            suggested = data.suggestion;
        });
    }else{
        $("#suggestion").html("");
    }
}

</script>