<div>
    <table>
        <tr>
            <th>English Phrase</th>
            <th>Dutch Translation</th>
            <th>Actions</th>
        </tr>

        <?php
            //List all the phrases and their translations (if they have one).
            foreach($phraseManager->getAll() as $phrase){
                echo '<tr>
                    <td>' . $phrase->english . '</td>
                    <td>' . $phrase->translation . '</td>
                    <td>
                        <a href="index.php?page=translate&phrase=' . $phrase->id . '">Edit Translation</a>
                    </td>
                </tr>';
            }
        ?>
    </table>
</div>