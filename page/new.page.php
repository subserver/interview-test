<div>
    <h3>Add New English Phrase</h3>
    <span id="similar-warning"></span><br>
    <textarea id="new-phrase-textarea" rows="4" cols="50" placeholder="English phrase..."></textarea><br>
    <button id="new-pharse-button" >Save</button>
</div>

<script>
    //Insert new phrase into database.
    $('#new-pharse-button').click((e) => {
        data = {
            phrase: $("#new-phrase-textarea").val()
        }

        $.post("ajax/newphrase.ajax.php", data ,function(data){
            alert("Phrase saved!");
        })
        .fail((xhr) => {
            alert("Failed to save new phrase!");
        });
    });


    //Detect changes on the text, check for suggestions on each keyup.
    //Should probably add a timer so it checks after a few keyups or ~500ms after typing stops.
    $("#new-phrase-textarea").on('keyup', function(e) {
        if($("#new-phrase-textarea").val().length > 5){
            $.post("ajax/fetchsuggestions.ajax.php", { phrase: $("#new-phrase-textarea").val() }, function(data){
                data = JSON.parse(data);
                $("#similar-warning").html("A similar item already exists: '" + data.english + "'");
            })
            .fail(() => {
                clearWarning();
            })
        }else{
            clearWarning();
        }
    });

    function clearWarning(){
        $("#similar-warning").html("");
    }
    

</script>