<?php
    class PhraseManager {
        private $dbConnection = null;

        function __construct(){
            $this->connectToDatabase();
        }

        /**
         * Retrieve all phrases and their translation.
         */
        public function getAll(){
            return $this->dbConnection->query("SELECT id, english, translation FROM phrase LEFT JOIN translation ON translation.phrase = phrase.id", PDO::FETCH_OBJ)->fetchAll();
        }

        /**
         * Retrieve a single phrase and translation given a phrase id.
         * @param($id) The id of the phrase to retireve
         */
        public function getById($id){
            $statement = $this->dbConnection->prepare("SELECT id, english, translation FROM phrase LEFT JOIN translation ON translation.phrase = phrase.id WHERE id = :phraseid");
            $statement->bindParam(':phraseid', $id);
            $statement->setFetchMode(PDO::FETCH_OBJ); 
            $statement->execute();
            return $statement->fetch();
        }

        /**
         * Save a translation
         * @param($phraseId) The phrase of the id to save the translation for
         * @param($translation) The translated text
         */
        public function saveTranslation($phraseId, $translation){
            $statement = $this->dbConnection->prepare("INSERT INTO translation (phrase, language, translation) VALUES (:phraseid, 1, :translation) ON DUPLICATE KEY UPDATE translation = :translation2");
            $statement->bindParam(':phraseid', $phraseId);
            $statement->bindParam(':translation', $translation);
            $statement->bindParam(':translation2', $translation);
            return $statement->execute();
        }

        /**
         * Insert a new phrase into the database
         * @param($phrase) The english phrase to insert
         */
        public function newPhrase($phrase){
            $statement = $this->dbConnection->prepare("INSERT INTO phrase (english) VALUES (:phrase)");
            $statement->bindParam(':phrase', $phrase);
            return $statement->execute();
        }


        /**
         * Returns a similar phrase and details.
         * @param($phrase) The phrase to compare with
         */
        public function findSimilarPhrase($phrase){
            $statement = $this->dbConnection->prepare("SELECT id, english FROM phrase WHERE LOWER(english) LIKE :phrase");
            $statement->bindValue(':phrase', '%' . $phrase . '%');
            $statement->setFetchMode(PDO::FETCH_OBJ); 
            $statement->execute();
            return $statement->fetch();
        }

        /**
         * Returns matching phrase details and translation
         * @param($phrase) The phrase to match
         */
        public function findPhraseTranslation($phrase){
            $statement = $this->dbConnection->prepare("SELECT id, english, translation FROM phrase LEFT JOIN translation ON translation.phrase = phrase.id WHERE LOWER(english) LIKE :phrase");
            $statement->bindParam(':phrase', $phrase);
            $statement->setFetchMode(PDO::FETCH_OBJ); 
            $statement->execute();
            return $statement->fetch();
        }


        /**
         * Establish a connection to the database.
         * Note: the connection is stored as an object variable $this->dbConnection.
         */
        private function connectToDatabase(){
            global $dbConnectionString, $dbUsername, $dbPassword;
            try {
                $this->dbConnection = new PDO($dbConnectionString, $dbUsername, $dbPassword);
                $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e){
                echo "Failed to connect to database: " . $e->getMessage();
                die();
            }
        }
    }
?>