<?php   
    if(!isset($_POST['phrase'])){
        http_response_code(400);
        die("A phrase to match is required");
    }

    //Import settings and manager class.
    require_once('../settings.php');
    require_once('../phraseManager.php');

    //Setup Model
    $phraseManager = new PhraseManager();

    //Extract the phrase from the query.
    $phrase = $_POST['phrase'];

    //Find matching phrases
    $suggestion = $phraseManager->findSimilarPhrase($phrase);

    if($suggestion != false){
        //Echo suggestions
        http_response_code(200);
        echo json_encode($suggestion);
    }
    else{
        http_response_code(404);
        echo "No suggestions";
    }

?>