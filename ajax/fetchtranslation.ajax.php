<?php   
    if(!isset($_POST['phrase'])){
        http_response_code(400);
        die("A phrase to translate is required");
    }

    //Import settings and manager class.
    require_once('../settings.php');
    require_once('../phraseManager.php');

    //Setup Model
    $phraseManager = new PhraseManager();

    //Extract the phrase from the query.
    $phrase = $_POST['phrase'];

    //Find matching phrase
    $translationDetails = $phraseManager->findPhraseTranslation($phrase);

    if($translationDetails == false){
        //No direct translation, find some suggestions and supply them with error message.
        $suggested = $phraseManager->findSimilarPhrase($phrase);
        if($suggested != false){
            http_response_code(404);
            $suggestion = [
                "suggestion" => $suggested
            ];
            echo json_encode($suggestion);
        }
        die();
    }
    else{
        //Echo translation details
        http_response_code(200);
        echo json_encode($translationDetails);
    }


?>