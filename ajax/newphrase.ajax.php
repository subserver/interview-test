<?php   
    if(!isset($_POST['phrase'])){
        http_response_code(400);
        die("Missing required parameters");
    }

    //Import settings and manager class.
    require_once('../settings.php');
    require_once('../phraseManager.php');

    //Setup Model
    $phraseManager = new PhraseManager();

    //Extract the required parameters
    $phrase = $_POST['phrase'];

    //Find matching phrase
    $success = $phraseManager->newPhrase($phrase);

    if($success){
        http_response_code(200);
    }else{
        http_response_code(500);
    }

    


?>