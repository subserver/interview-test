<?php   
    if(!isset($_POST['phrase']) || !isset($_POST['translation'])){
        http_response_code(400);
        die("Missing required parameters");
    }

    //Import settings and manager class.
    require_once('../settings.php');
    require_once('../phraseManager.php');

    //Setup Model
    $phraseManager = new PhraseManager();

    //Extract the required parameters
    $phraseId = $_POST['phrase'];
    $translation = $_POST['translation'];

    //Find matching phrase
    $success = $phraseManager->saveTranslation($phraseId, $translation);

    if($success){
        http_response_code(200);
    }else{
        http_response_code(500);
    }

    


?>