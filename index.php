<?php
    //Import settings and manager class.
    require_once('settings.php');
    require_once('phraseManager.php');

    //Setup Model
    $phraseManager = new PhraseManager();

    //Pages that are allowed to be loaded. This is a cheapish workaround due to time limitations.
    $safePages = ["main", "list", "new", "translate"];

    //Extract the page to load from the request.
    $pageToLoad = (isset($_GET['page']) ? $_GET['page'] : "main");

    //Check that the requested page is safe to load.
    if(!in_array($pageToLoad, $safePages)){
        http_response_code(404);
        include("page/404.page.php");
        die();
    }
?>

<html>
    <script src="assets/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/style.css" />
    <div id="container">
        <div id="header">
            <nav>
                <a href="index.php?page=main">Home</a> | 
                <a href="index.php?page=list">All Phrases</a> | 
                <a href="index.php?page=new">New Phrase</a>
            </nav>
        </div>
        <div id="contents">
                <?php
                    //Include the page contents that we want to display.
                    include('page/' . $pageToLoad . '.page.php');
                ?>
        </div>
    </div>
</html>